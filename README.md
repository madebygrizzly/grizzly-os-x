# [Grizzly OS X](http://madebygrizzly.com)

Grizzly's Mac OS X Image

## Getting Started

* Create a new Mac OS X account
* The **Full Name** should be your first and last name and the **Account Name** should be the same as the first part of the grizzly email. So, if the email is `hello@madebygrizzly.com`, the Account Name should say `hello`

## Dev Tools

* **Xcode** - `xcode-select --install`
* **Homebrew** - `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
    * Install Homebrew Dupes - `brew tap homebrew/dupes`
    * Install Homebrew Versions - `brew tap homebrew/versions`
    * Install Homebrew Cask - `brew tap caskroom/cask`
* **GNU sed** - `brew install gnu-sed --with-default-names`
* **Git** - `brew install git`
* **ssh-copy-id** - `brew install ssh-copy-id`
* **Zsh** - `brew install zsh`
    * Switch to Zsh - `chsh -s /bin/zsh`
* **Oh My Zsh** - `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
    * `~/.zshrc` Recommendations
        * `ZSH_THEME="agnoster"`
        * `plugins=(git zsh-autosuggestions zsh-syntax-highlighting)`
        * `DEFAULT_USER=yourusername`
* **PHP 7.2** - `brew install php72`
    * Export path - `export PATH="$(brew --prefix php72)/bin:$PATH"`
* **PHPUnit** - `brew install phpunit`
* **PHP_CodeSniffer** - `brew install php-code-sniffer`
* **PHP-CS-Fixer** - `brew install php-cs-fixer`
* **Composer** - `brew install composer`
* **WP-CLI** - `brew install wp-cli`
* **NVM & Node.js** - `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash`
    * Restart your terminal
    * Install node - `nvm install v9.11.1`
    * Set as default - `nvm alias default v9.11.1`
* **RVM & Ruby 2.4.3** - `\curl -sSL https://get.rvm.io | bash -s stable --ruby=2.4.3`

## Text Editors / IDEs

### Visual Studio Code

#### Extensions

* Align
* Beautify
* change-case
* Docker
* EditorConfig
* ESLint
* HTML Snippets
* Jekyll Snippets
* Jekyll Syntax Support
* Lodash Snippets
* Mustache
* PHP DocBlocker
* PHP IntelliSense
* phpunit snippets
* sftp
* TSLint
* TWIG pack
* TypeScript Importer
* VS Code CSS Comments
* Wordpress Snippet

### Atom

#### Packages

* editorconfig
* emmet
* aligner
* atom-beautify
* linter-jshint
* linter-eslint
* file-icons
* compare-files
* split-diff
* docblockr
* atom-ternjs
* gsap-snippets
* atom-autocomplete-php
* atom-wordpress
* autocomplete-wordpress-hooks
* minimap ([Key Bindings](https://github.com/atom-minimap/minimap#key-bindings), Auto Toggle off)
* minimap-git-diff
* minimap-split-diff
* markdown-preview-plus
* section-comment-snippets
* atom-jade
* language-pug
* remote-ftp

### Sublime Text

#### Packages

* [Package Control](https://packagecontrol.io/)
* Editor​Config
* Emmet
* DocBlockr
* Dash Doc
* AdvancedNewFile
* SidebarEnhancements
* HTML Mustache
* ACF Snippets
* SCSS
* CSS Extended Completions
* CodeKit
* jQuery
* Gist
* JavaScript Completion
* GSAP Snippets
* WordPress
* SFTP

## Command Line
* [iTerm](https://www.iterm2.com/)
* [Suggested iTerm/Zsh Settings](https://gist.github.com/kevin-smets/8568070)

## Apps to Be Installed (From the Web or via Homebrew Cask)

* [Docker](https://www.docker.com/products/docker)
* [Sequel Pro](https://www.sequelpro.com/)
* [CodeKit](https://incident57.com/codekit/)
* [Postman](https://www.getpostman.com/)
* [Chrome](https://www.google.com/chrome/)
* [Firefox](https://www.mozilla.org/)
* [Opera](http://www.opera.com/)
* [Harvest](https://www.getharvest.com/)
* [LastPass](https://lastpass.com/misc_download2.php)
* [Sketch](https://www.sketchapp.com/)
* [Sketch Toolbox](http://sketchtoolbox.com/)
* [Craft](https://www.invisionapp.com/craft)
* [IconJar](http://geticonjar.com/)
* [Adobe Creative Cloud](http://www.adobe.com/)
* [Carbon Copy Cloner](https://bombich.com/)
* [Dash](https://kapeli.com/dash)
* [Sonos](http://www.sonos.com/en-us/support)
* [MacDown](http://macdown.uranusjr.com/)

## Apps to Be Installed (Mac App Store)

* Slack
* Little Ipsum
* Moom
* iWork
* CopyClip
* Alfred

## Suggested Chrome Extensions

* AngularJS Batarang
* BugHerd Plus
* ColorZilla
* JSON View
* Mixmax
* ng-inspector for AngularJS
* Page Speed Insights
* React Developer Tools
* The Great Suspender
* Wappalyzer
* Web Developer
* Web Developer Form Filler
* WhatFont
* Window Resizer
* [yslow](http://yslow.org/)

## Final Steps

* Set hostname - `sudo hostname -s madebygrizzly.com`
* Configure Git - `git config --global user.name "$(whoami)" && git config --global user.email "$(whoami)@$(hostname)" && git config --global push.default simple`
* Generate SSH Key - `ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname)"`
* For software licenses and steps for connecting to the Grizzly server, please refer to the [internal setup documentation](https://docs.google.com/document/d/1zuVTVzsDH3habuY41ekV7J_hPZFp7Cg9RPKPt46n6kA)

## Resources

### Homebrew Commands

* `brew cask install [app-name]` - Install a Mac OS X app
* `brew cask uninstall [app-name]` - Uninstall a Mac OS X app
* `brew cask list` - Show all apps installed with Hombrew Cask
* `brew search [package-name]` - Search for a package
* `brew install [package-name]` - Install a package
* `brew uninstall [package-name]` - Uninstall a package
* `brew list` - Show all installed packages
* `brew list --versions [package-name]` - Show version of an installed package
* `brew list --versions` - Show versions of all installed packages
* `brew home [package-name]` - Open homepage for package
* `brew unlink [package-name]` - Unlink a package
* `brew link [package-name]` - Link a package
* `brew switch [package-name] [version]` - Switch versions of a package
* `brew info [package-name]` - List versions, caveats, etc.
* `brew cleanup [package-name]` - Remove old versions of a package
* `brew update` - Update Homebrew
* `brew outdated` - Show packages that can be upgraded
* `brew upgrade [package-name]` - Upgrade a package
* `brew doctor` - Show warnings for troubleshooting purposes
